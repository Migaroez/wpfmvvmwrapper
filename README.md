# README #

### What is this repository for? ###

Inspired by the way mvvmcross accelerates app development for cross platform apps using xamarin pcl, I am combining all the little things i do when setting up a normal Wpf Mvvm application with some useful functions and pre implemented patterns.

In the end this repository will contain all the small tools that I use frequently in wpf mvvm projects, including but not limited to:

* Automatic view locator trough reflection.
* Automatic service locator trough reflection.
* Implementation of Icommand, normal and async (currently uses the apex library).
* Common IValueConverters.
* Small custom IoC container (currently uses the structuremap library) (Maybe, structuremap is actually really really good).
* Winforms style InputBox and MessageBox that complies with mvvm.

### How do I get set up? ###

* Create a new WPF application in visual studio
* Remove MainWindow.xaml
* Get the NuGet package
* Accept the changes to app.xaml and app.xaml.cs