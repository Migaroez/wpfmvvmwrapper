using StructureMap;
using System.Windows;
using WpfMvvmWrapper;
using WpfMvvmWrapper.StructureMapExtensions;
using $rootnamespace$.ViewModels;

namespace $rootnamespace$
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            // Initialize a StructureMap IoC Container and define the ViewManager
            // You can use a custom IViewManager implementation here.
            // Add all your dependencies to this container to be able to utilize them easily from Program.
            IContainer container = new Container(c =>
            {
                // Manually Registration
                c.For<IViewManager>().Use<ViewManagerWpf>();

                // Tell structurmap to scan an assembly for services (they end in "Service");
                // Else we would have to register every service like so:
                // c.For<ITestService>().Use<TestService>().Singleton();
                c.Scan(s =>
                {
                    s.Assembly("$rootnamespace$");
                    s.Convention<ServiceAffixConvention>();
                });
            });

            // Initialize the wrapper
            Program.Initialize(container);

            // Show the first viewmodel
            Program.ShowViewModel<MainViewModel>();
        }
    }
}