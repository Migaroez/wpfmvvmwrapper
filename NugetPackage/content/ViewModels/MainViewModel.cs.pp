using Apex.MVVM;
using WpfMvvmWrapper;
using $rootnamespace$.Services;

namespace $rootnamespace$.ViewModels
{
	
    public class MainViewModel : UIDiscoverable
    {
		// UIDiscoverable implements INotifyPropertyChanged
        // Can be used just with NotifyPropertyChanged() and it will use the invoking members name
        // Can also be used as ( ()=> Property) ie: NotifyPropertyChanged( ()=> Test);
        private string test = "WpfMvvmWrapper with Ioc working correctly.";
        public string Test { get { return test; } set { test = value; NotifyPropertyChanged(); } }

        private ITestService _Service;
        public ITestService Service { get { return _Service; } set { _Service = value; NotifyPropertyChanged(); } }
        public MainViewModel(ITestService service)
        {
            Service = service;
        }

        private Command _DoubleWindowCommand;
        public Command DoubleWindowCommand
        {
            get
            {
                _DoubleWindowCommand = _DoubleWindowCommand ?? new Command(DoDoubleWindowCommand);
                return _DoubleWindowCommand;
            }
        }

        private void DoDoubleWindowCommand()
        {
            Program.ShowViewModel<MainViewModel>(true);
        }
    }
}