using WpfMvvmWrapper;

namespace $rootnamespace$.Services
{
    public class TestService : UIDiscoverable, ITestService
    {
        private string _StringProperty;
        public string StringProperty { get { return _StringProperty; } set { _StringProperty = value; NotifyPropertyChanged(); } }
    }
}