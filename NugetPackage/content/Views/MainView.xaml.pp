<Window x:Class="$rootnamespace$.Views.MainView"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        mc:Ignorable="d"
        Title="MainWindow" Height="350" Width="525">
    <Grid>
        <StackPanel Orientation="Vertical" Margin="3">
            <TextBlock Text="This text is bound to the instance of he viewmodel, it will not be transfered to other copies of this viewmodel." TextWrapping="Wrap"/>
            <TextBox Text="{Binding Test, Mode=TwoWay}" Margin="20,0,0,0" Background="LightGray"/>

            <TextBlock Text="This text is bound to the service used by the viewmodel, it will be transfered to other viewmodels using the same service." TextWrapping="Wrap" Margin="0,8,0,0"/>
            <TextBox Text="{Binding Service.StringProperty, Mode=TwoWay}" Margin="20,0,0,0" Background="LightGray"/>

            <Button Content="Duplicate viewmodel" VerticalAlignment="Center" Command="{Binding DoubleWindowCommand}" Margin="20,5" />
        </StackPanel>
    </Grid>
</Window>
