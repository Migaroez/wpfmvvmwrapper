﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using StructureMap;

namespace WpfMvvmWrapper
{
    /// <summary>
    /// Implements IViewManager to show viewmodels in a wpf UI
    /// On creation it loads all views, see LoadViews() for details.
    /// </summary>
    public class ViewManagerWpf : IViewManager
    {
        private IContainer container;

        public string ViewAffix { get; set; }
        public string ViewModelAffix { get; set; }

        private List<Type> viewList;
        private List<Type> ViewList { get { if (viewList == null) viewList = new List<Type>(); return viewList; } }

        /// <summary>
        /// Initializes a new instace of ViewManagerWpf
        /// </summary>
        /// <param name="viewAffix">The part at the end of your views that they all have in common.</param>
        /// <param name="viewModelAffix">The part at the end of your viewmodels that they all have in common.</param>
        public ViewManagerWpf(IContainer container, string viewAffix = "View", string viewModelAffix = "ViewModel")
        {
            if (container == null)
                throw new NullReferenceException("The ViewManager needs a container containing all dependencies the viewmodels need.");
            this.container = container;
            ViewAffix = viewAffix;
            ViewModelAffix = viewModelAffix;
            LoadViews();
        }

        /// <summary>
        /// Takes a viewmodel type and tries to find the view equivalent in the list loaded previously.
        /// If the view is of type window, it will display it normally or as a dialog.
        /// </summary>
        /// <typeparam name="viewModelType"></typeparam>
        /// <param name="asPopUp">Wether to display the view as a dialog.</param>
        /// <returns>The created viewModel or Null</returns>
        public object ShowViewModel<viewModelType>(bool asPopUp = false)
        {
            Type viewType = ResolveView<viewModelType>();
            Type vmType = typeof(viewModelType);

            if (viewType.IsSubclassOf(typeof(Window)))
            {
                var view = Activator.CreateInstance(viewType);
                var viewModel = container.GetInstance(vmType); //Let the IoC Container build up the neccesary dependencies.
                ((Window)view).DataContext = viewModel;
                if (!asPopUp)
                    ((Window)view).Show();
                else
                    ((Window)view).ShowDialog();

                return viewModel;
            }
            else
                throw new NotImplementedException();
        }

        private Type ResolveView<viewModel>()
        {
            string typename = typeof(viewModel).ToString();
            typename = typename.Replace(ViewModelAffix, ViewAffix);
            Type returnType = ViewList.FirstOrDefault(t => t.ToString() == typename); //Search the types incovered by reflection
            // Type.GetType(typename, true, true); //? Is there a better way to do this, like searching all loaded asemblies for ViewName instead of in a certain namespace in the current assembly.
            return returnType;
        }

        /// <summary>
        /// Scans (multithreaded) all loaded assemblies for namespaces that end in ".Views" and searches for types that end in the ViewAffix.
        /// Then adds the found types to a list for later access.
        /// </summary>
        public void LoadViews() //Reflection power!
        {
            ////- SingleThreaded
            //foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            //{
            //    foreach (Type t in a.GetTypes())
            //    {
            //        if (t.Name.EndsWith(ViewAffix) && t.FullName.Contains(".Views."))
            //            ViewList.Add(t);
            //    }
            //}

            //MultiThreaded
            Parallel.ForEach(AppDomain.CurrentDomain.GetAssemblies(), (assembly) =>
            {
                Parallel.ForEach(assembly.GetTypes(), (type) =>
                {
                    if (type.FullName.Contains(".Views.") && type.Name.EndsWith(ViewAffix))
                        ViewList.Add(type);
                });
            });
        }
    }
}
