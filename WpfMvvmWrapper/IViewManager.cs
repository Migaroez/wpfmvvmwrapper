﻿namespace WpfMvvmWrapper
{
    public interface IViewManager
    {
        string ViewAffix { get; set; }
        string ViewModelAffix { get; set; }

        void LoadViews();
        object ShowViewModel<viewModelType>(bool asPopUp = false);
    }
}