﻿using System.Collections.Generic;

namespace WpfMvvmWrapper.Settings
{
    public interface ISettingManager
    {
        string FileExtension { get; set; }
        bool OverRideExistingGroupOnLoad { get; set; }
        Dictionary<string, SettingGroup> settingGroups { get; set; }

        void AddSettingGroup(SettingGroup settingGroup);
        SettingGroup GetSettingGroup(string key);
        SettingGroup GetSettingGroup(string type, string name);
        void LoadAll();
        void LoadSettingGroup(string filePathOverride);
        void LoadSettingGroupType(string type);
        void RemoveSettingGroup(string key);
        void RemoveSettingGroup(string type, string name);
        void SaveAll();
        void SaveSettingGroup(string name, string type = "");
        void SaveSettingGroupType(string type);
    }
}