﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WpfMvvmWrapper.Settings
{
    public class Setting
    {
        private string _Value;
        public string Value { get { return _Value; } set { SetValue(value); } }

        private string _OldValue;
        public string OldValue { get { return _OldValue; } }

        private void SetValue(string value)
        {
            _OldValue = Value;
            _Value = value;
        }

        public void Revert()
        {
            string temp = Value;
            _Value = _OldValue;
            _OldValue = temp;
        }

        public Setting (string value)
        {
            Value = value;
        }
    }
}
