﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WpfMvvmWrapper.Settings
{
    public class SettingGroup
    {
        private Dictionary<string, object> _Settings;
        public Dictionary<string, object> Settings { get { if (_Settings == null) _Settings = new Dictionary<string, Object>(); return _Settings; } set { _Settings = value; } }

        public string Name { get; set; }

        public string Type { get; set; }

        public string FileNameOverride { get; set; }

        public bool IsDirty { get; private set; } = false;

        /// <summary>
        /// Either sets a setting that already exists, or creates a new one with the given values.
        /// Afterwards, it sets IsDirty to true (marking that this Group needs to be saved.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetSetting(string key, object value)
        {
            if (Settings.Where(itm => itm.Key == key).Count() > 0) //Remove the key if it already exists
                Settings.Remove(key);
            Settings.Add(key, value);

            IsDirty = true;
        }

        public object GetSetting(string key)
        {
            if (Settings.Where(itm => itm.Key == key).Count() > 0)
                return Settings[key];
            return null;
        }

        public void ResetDirty()
        {
            IsDirty = false;
        }
    }
}
