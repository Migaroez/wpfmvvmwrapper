﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using WpfMvvmWrapper;

namespace WpfMvvmWrapper.Settings
{
    public class SettingManager : UIDiscoverable, ISettingManager
    {
        private string _basePath;
        private string basePath { get { if (_basePath == null || _basePath.Length < 1) _basePath = GetBasePath(); return _basePath; } }

        private Dictionary<string, SettingGroup> _settingGroups;
        public Dictionary<string, SettingGroup> settingGroups { get { if (_settingGroups == null) _settingGroups = new Dictionary<string, SettingGroup>(); return _settingGroups; } set { _settingGroups = value; } }

        private string _FileExtension = "";
        public string FileExtension { get { return _FileExtension; } set { _FileExtension = value; NotifyPropertyChanged(); } }

        private bool _OverRideExistingGroupOnLoad = false;
        public bool OverRideExistingGroupOnLoad { get { return _OverRideExistingGroupOnLoad; } set { _OverRideExistingGroupOnLoad = value; NotifyPropertyChanged(); } }

        private JsonSerializer serializer;

        public SettingManager()
        {
            serializer = new JsonSerializer();
            serializer.NullValueHandling = NullValueHandling.Ignore;
            serializer.TypeNameHandling = TypeNameHandling.Auto;
            serializer.Formatting = Formatting.Indented;
        }

        public SettingGroup GetSettingGroup(string key)
        {
            if(settingGroups.ContainsKey(key))
                return settingGroups[key];
            return null;
        }

        public SettingGroup GetSettingGroup(string type, string name)
        {
            return GetSettingGroup(BuildKey(type, name));
        }

        /// <summary>
        /// Adds a new settingGroup to the Settings list, the key will be .Type + "-" + .Name
        /// </summary>
        /// <param name="settingGroup"></param>
        public void AddSettingGroup(SettingGroup settingGroup)
        {
            if (settingGroups.Where(a => a.Value.Type == settingGroup.Type).Where(b => b.Value.Name == settingGroup.Name).Count() == 0)
            {
                settingGroups.Add(BuildKey(settingGroup.Type,settingGroup.Name), settingGroup);
            }
            else
            {
                settingGroups[BuildKey(settingGroup.Type,settingGroup.Name)] = settingGroup;
            }
        }

        public void RemoveSettingGroup(string key)
        {
            if(settingGroups.ContainsKey(key))
                settingGroups.Remove(key);
        }

        public void RemoveSettingGroup(string type, string name)
        {
            RemoveSettingGroup(BuildKey(type,name));
        }

        public void LoadSettingGroup(string filePathOverride)
        {
            Load(filePathOverride);
        }

        public void LoadSettingGroupType(string type)
        {
            // look for files in a folder called /settings/typename that have extension FileExtension
            // If no fileExtension is defined, look pure on the name.
            // For every file found, try to deserialize it and create a new SettingGroup in the Settings Dictionary
            string path = GetBasePath();
            path = Path.Combine(path, type);
            LoadFolder(path);
        }

        public void LoadAll()
        {
            // Look inside the Settings folder
            // for each file with the defined extension found in that folder, try and deserialize and add to the SettingsDic
            // repeat for every subfolder
            LoadFolder(basePath);
        }

        public void SaveSettingGroup(string name, string type = "")
        {
            List<SettingGroup> temp;
            if (type.Length > 0)
                temp = settingGroups.Values.Where(itm => itm.Type == type).ToList();
            else
                temp = settingGroups.Values.ToList();

            foreach (SettingGroup item in temp.Where(itm => itm.Name == name))
            {
                Save(item);
            }
        }

        public void SaveSettingGroupType(string type)
        {
            foreach (SettingGroup group in settingGroups.Values.Where(itm=>itm.Type == type && itm.IsDirty))
            {
                Save(group);
            }
        }

        public void SaveAll()
        {
            foreach (SettingGroup setting in settingGroups.Values)
            {
                if(setting.IsDirty)
                    Save(setting);
            }
        }

        private void Save(SettingGroup group)
        {
            if(group!= null)
            {
                group.ResetDirty();

                string path = basePath;
                if (group.FileNameOverride != null && group.FileNameOverride.Length > 0)
                    path = group.FileNameOverride;
                else
                {
                    if (group.Type != null)
                        path = Path.Combine(path, group.Type);
                    CheckDirectory(path);
                    path = Path.Combine(path, group.Name + FileExtension);
                }


                try
                {
                    using (StreamWriter sw = new StreamWriter(path))
                    using (JsonWriter writer = new JsonTextWriter(sw))
                    {
                        serializer.Serialize(writer, group);
                    }
                }
                catch (Exception)
                {
                    //-
                    Debug.Print("Medium: Could not save settingGroup " + BuildKey(group.Type, group.Name) + " to " + path);
                }
            }
        }

        private void Load(string filePath, bool overRideFilePath = false)
        {
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                using (JsonTextReader reader = new JsonTextReader(sr))
                {
                    SettingGroup temp = serializer.Deserialize<SettingGroup>(reader);
                    if (overRideFilePath)
                        temp.FileNameOverride = filePath;
                    if(settingGroups.Where(a=>a.Value.Type == temp.Type).Where(b=>b.Value.Name==temp.Name).Count() == 0)
                        AddSettingGroup(temp);
                    else if(OverRideExistingGroupOnLoad)
                    {
                        settingGroups.Remove(BuildKey(temp.Type,temp.Name));
                        AddSettingGroup(temp);
                        //-
                        Debug.Print("Double settings file detected and previous one overwritten: " + filePath);
                    }
                    else
                    {
                        //-
                        Debug.Print("Double settings file detected and ignored: " + filePath);
                    }
                }
            }
            catch (Exception)
            {
                //- 
                Debug.WriteLine("Cannot load settings file: " + filePath);
            }      
        }

        private void LoadFolder(string folderPath)
        {
            foreach (string file in Directory.GetFiles(folderPath))
            {
                if (file.Substring(file.Length - 1 + FileExtension.Length, FileExtension.Length) == FileExtension)
                {
                    Load(file);
                }
            }

            foreach (string dir in Directory.GetDirectories(folderPath))
            {
                LoadFolder(dir);
            }
        }

        private void CheckDirectory(string directory)
        {
            ///If the folder does not exist, create it and change permisions.
            if (Directory.Exists(directory) == false)
            {
                Directory.CreateDirectory(directory);

                //If the folder did not yet exist, change permissions
                DirectorySecurity sec = Directory.GetAccessControl(directory);

                //Using this instead of the "Everyone" string means we work on non-English systems.
                SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);

                sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                Directory.SetAccessControl(directory, sec);
            }
        }

        private string GetBasePath()
        {
            Assembly entryAssem = Assembly.GetEntryAssembly();
            string company = "";
            object[] attribs = entryAssem.GetCustomAttributes(typeof(AssemblyCompanyAttribute), true);
            if (attribs.Length > 0)
            {
                company = ((AssemblyCompanyAttribute)attribs[0]).Company;
            }

            string result = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            if (company.Length > 0)
                result = Path.Combine(result, company);
            result = Path.Combine(result, entryAssem.GetName().Name,"Settings");
            return result;
        }

        private string BuildKey(string type, string name)
        {
            return type + "-" + name;
        }
    }
}
