﻿using StructureMap.Graph;
using StructureMap;
using StructureMap.Graph.Scanning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMvvmWrapper.StructureMapExtensions
{
    public class ServiceAffixConvention : IRegistrationConvention
    {
        public void ScanTypes(TypeSet types, Registry registry)
        {
            // Only work on concrete types
            foreach (Type type in types.FindTypes(TypeClassification.All))
            {
                if (type.Name.EndsWith("Service"))
                {
                    foreach (var _interface in type.GetInterfaces())
                    {
                        registry.For(_interface).Use(type).Singleton();
                    }
                }
            }
        }
    }
}
