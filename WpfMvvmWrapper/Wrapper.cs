﻿using StructureMap;
using System;
using System.Collections.Generic;
using WpfMvvmWrapper.Settings;

namespace WpfMvvmWrapper
{
    /// <summary>
    /// Singleton class that contains members and methods to perform Program level functions.
    /// Use Initialize(IContainer) before using any other methods. This Container needs an implementation of IViewManager.
    /// </summary>
    public class Wrapper
    {
        #region Properties
        private static Wrapper instance;
        public static Wrapper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Wrapper();
                }
                return instance;
            }
        }

        private IViewManager viewManager;
        public IViewManager ViewManager
        {
            get
            {
                if (!initilized)
                {
                    throw new NullReferenceException("Please initialize before using.");
                }
                else
                    return viewManager;
            }
        }

        private IContainer iocContainer;
        public IContainer IocContainer
        {
            get
            {
                if (!initilized)
                {
                    throw new NullReferenceException("Please initialize before using.");
                }
                else
                    return iocContainer;
            }
        }

        private ISettingManager settingManager;
        public ISettingManager SettingManager
        {
            get
            {
                if (!initilized)
                {
                    throw new NullReferenceException("Please initialize before using.");
                }
                else
                    return settingManager;
            }
        }
        #endregion
        private bool initilized;
        #region Fields

        #endregion

        #region Core Functions 
        /// <summary>
        /// Inizilize the Singleton for static function access.
        /// </summary>
        /// <param name="container">container that contains an IViewManager implementation</param>
        public static void Initialize(IContainer container)
        {
            Instance.iocContainer = container;
            Instance.viewManager = Instance.iocContainer.GetInstance<IViewManager>();

            Instance.settingManager = Instance.iocContainer.GetInstance<ISettingManager>();
            if (Instance.settingManager != null)
                Instance.settingManager.LoadAll();
            Instance.initilized = true;
        }
        #endregion

        #region MvvM Functions
        /// <summary>
        /// Asks the Implemented IViewManager to display the specified ViewModel
        /// </summary>
        /// <typeparam name="viewModel">The type of viewmodel you want to show</typeparam>
        /// <param name="asPopup">Change to true to show the viewmodel as a dialog interface.</param>
        public static object ShowViewModel<viewModel>(bool asPopup = false)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                return Instance.ViewManager.ShowViewModel<viewModel>(asPopup);
        }
        #endregion

        #region Setting Functions
        public static void SettingsAddGroup(SettingGroup settingGroup)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                Instance.SettingManager.AddSettingGroup(settingGroup);
        }

        public static SettingGroup SettingsGetGroup(string key)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                return Instance.SettingManager.GetSettingGroup(key);
        }
        public static SettingGroup SettingsGetGroup(string type, string name)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                return Instance.SettingManager.GetSettingGroup(type, name);
        }
        public static object SettingsGetSetting(string groupType, string groupName, string settingName)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
            {
                SettingGroup temp = Instance.SettingManager.GetSettingGroup(groupType, groupName);
                if (temp != null)
                    return temp.GetSetting(settingName);
                return null;
            }
        }

        public static void SettingsLoadAll()
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                Instance.SettingManager.LoadAll();
        }

        public static void SettingsLoadGroup(string filePathOverride)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                Instance.SettingManager.LoadSettingGroup(filePathOverride);
        }

        public static void SettingsLoadGroupType(string type)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                Instance.SettingManager.LoadSettingGroupType(type);
        }

        public static void SettingsRemoveGroup(string key)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                Instance.SettingManager.RemoveSettingGroup(key);
        }

        public static void SettingsRemoveGroup(string type, string name)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                Instance.SettingManager.RemoveSettingGroup(type, name);
        }

        public static void SettingsSaveAll()
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                Instance.SettingManager.SaveAll();
        }

        public static void SettingsSaveGroup(string name, string type = "")
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                Instance.SettingManager.SaveSettingGroup(name, type);
        }

        public static void SettingsSaveGroupType(string type)
        {
            if (!Instance.initilized)
            {
                throw new NullReferenceException("Please initialize before using.");
            }
            else
                Instance.SettingManager.SaveSettingGroupType(type);
        }
        #endregion
    }
}
