﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfMvvmWrapper
{
    /// <summary>
    /// Implements the INotifyProperty pattern.
    /// </summary>
    public class UIDiscoverable : INotifyPropertyChanged
    {
        // Base PropertyChangedEventHandler handler with CallerMemberName subsitution.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notfies the UI that the given property has changed.
        /// </summary>
        /// <param name="propertyName">Optional name of the property that the UI needs to notified of. If empty, takes the name of the invoking member.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Notfies the UI that the given property has changed.
        /// </summary>
        /// <typeparam name="T">The property in question ie: NotifyPropertyChanged( ()=> Test);</typeparam>
        /// <param name="property"></param>
        protected void NotifyPropertyChanged<T>(Expression<Func<T>> property)
        {
            string name = (property.Body as MemberExpression).Member.Name;
            NotifyPropertyChanged(name);
        }
    }
}
